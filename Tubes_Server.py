import math
from xmlrpc.server import SimpleXMLRPCServer
from xmlrpc.server import SimpleXMLRPCRequestHandler

class RequestHandler(SimpleXMLRPCRequestHandler):
	rpc_paths = ('/RPC2',)

address = ("localhost", 12345)

with SimpleXMLRPCServer(address, requestHandler = RequestHandler) as s:
	s.register_introspection_functions()
	class BlockingConcept:
		@staticmethod
		def fixedBfr(block_size, record_size):
			return math.floor(block_size / record_size)
		@staticmethod
		def W_fixedBfr(bfr,gap):
			return math.floor(gap / bfr)
		@staticmethod
		def spannedBfr(block_size, record_size, pointer_size, marker_size):
			return math.floor((block_size-pointer_size) / (record_size+marker_size))
			
		@staticmethod
		def W_spannedBfr(pointer_size,marker_size,gap,bfr):
			return math.floor(marker_size+((pointer_size+gap)/bfr))
		@staticmethod
		def unspannedBfr(block_size, record_size, marker_size):
			return math.floor((block_size-(0.5*record_size)) / (record_size+marker_size))
		
		@staticmethod
		def W_unspannedBfr(marker_size,record_size,gap,bfr):
			return math.floor((marker_size+((0.5*record_size)+gap) /bfr ))
		
		@staticmethod
		def jumlahBlock(jml_record, bfr):
			return math.ceil(jml_record / bfr)

		@staticmethod
		def bulkTransferrate(transfer_rate, record_size, w):
			return (transfer_rate/0.5)*(record_size /(record_size+w))

		@staticmethod
		def recordTransferTime(record_size, transfer_rate):
			return record_size/transfer_rate

		@staticmethod
		def blockTransferTime(block_size, transfer_rate):
			return block_size/transfer_rate

		@staticmethod
		def fanoutRasio(block_size, pointer_size, key_size):
			return math.floor(block_size / (pointer_size + key_size))

		@staticmethod
		def jumlahIndex (jml_record, fanout):
			return math.ceil(jml_record / fanout)

	s.register_instance(BlockingConcept())
	s.serve_forever()
