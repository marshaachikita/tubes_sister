# TUBES_SISTER
# Blocking Concept menggunakan RPC

1. Buka file source code ‘Tubes_Server.py’ menggunakan bahasa pemrograman python 3
2. Buka file source code ‘Tubes_Client.py’ menggunakan bahasa pemrograman python 3
3. Running file ‘Tubes_Server.py’ pada komputer A
4. Running file ‘Tubes_Client.py’ pada komputer B
5. Pada file ‘Tubes_Client.py’ sistem akan menampilkan 3 fungsi utama, yaitu :
    * Fixed Blocking
    * Variabel Length Spanned
    * Variabel Length Unspanned

![gambar1](Hasil/1.png)

Pilih menu yang diinginkan, lalu sistem akan menampilkan inputan yang harus di isi dan akan menampilkan hasil perhitung Bfr.

![gambar2](Hasil/2.png)

Jika ingin mengetahui jumlah block maka inputkan ‘Y’ atau ‘y’ untuk menghitung jumlah block yang dicari.

![gambar3](Hasil/3.png)

## Sequence Diagram

* Fixed Blocking

![gambar1](report_image/FixedBfr.jpg)

* Variable Spanned Blocking

![gambar1](report_image/Variable Spanned Blocking.png)

* Variable Unspanned Blocking

![gambar1](report_image/Variable Unspanned Blocking .png)

## COTS

## 1. Perhitungan Fanout Rasio

Jika ingin menghitung nilai fanout rasio untuk menyimpan sebuah entri indeks, maka pilih menu nomor 4.

![4](/uploads/ec5f75f8c0466c7bf0cfc2e5897b8b50/4.JPG)

Kemudian sistem akan menampilkan inputan yang harus diisi dan akan menampilkan hasil perhitungan fanout rasio 

![6](/uploads/a048c8d8fa25b9ef74bf5f9b90640722/6.JPG)

Jika ingin menghitung block indeks maka inputkan 'Y' atau 'y' jika tidak inputkan 'T' atau 't'

![7](/uploads/c0bbad113e6dfde8e58fc50d31f1b192/7.JPG)

## 2. Perhitungan Wasted Storage (Pemborosan Ruang)
Jika ingin menghitung nilai pemborosan ruang, maka pilih menu nomor 2 pada submenu di setiap menu utama.
*  Wasted Strorage Fixed Blocking
![alt text](cots_runimage/Wfixedblocking.PNG)
*  Wasted Strorage Variable Spenned Blocking
![alt text](cots_runimage/Wspennedblocking.PNG)
*  Wasted Strorage Variable Unspenned Blocking
![alt text](cots_runimage/Wunspennedblocking.PNG)

## 3. Perhitungan Bulk Transfer Rate
Jika ingin menghitung nilai bulk transfer rate, setelah menghitung pemborosan ruang maka pilih 'Y' lalu sistem akan menampilkan inputan yang harus di isi dan akan menampilkan hasil perhitung Bulk Transfer Rate.
*  Bulk Transfer Rate Fixed Blocking
![4](Hasil/4.JPG)
*  Bulk Transfer Rate Variable Spenned Blocking
![5](Hasil/5.JPG)
*  Bulk Transfer Rate Variable Unspenned Blocking
![6](Hasil/6.JPG)

## 4. Perhitungan Record Transfer Time (Tr) dan Block Transfer Time (btt)
Untuk menghitung nilai Tr dan btt pada program tersebut, pilih nomor 3 pada sub-menu yang ada
![cots_new](/uploads/65801674a242ede4ea8c952be5d668f8/cots_new.PNG)
