import xmlrpc.client

server = xmlrpc.client.ServerProxy('http://localhost:12345')

def show_menu():
	print("\n======================================================")
	print("|                  Blocking Concept                  |")
	print("======================================================")
	print("| 1. Fixed Blocking                                  |")
	print("| 2. Variable Spanned Blocking                       |")
	print("| 3. Variable Unspanned Blocking                     |")
	print("| 4. Fanout Rasio			                        |")
	print("| 5. Exit                                            |")
	print("======================================================")
	pilihan = input("Masukkan Pilihan: ")

	if pilihan == '1':
		input1 = input("\nMasukkan ukuran block (dalam byte): ")
		block_size = int(input1)
		input2 = input("Masukkan ukuran record (dalam byte): ")
		record_size = int(input2)
		
		bfr = server.fixedBfr(block_size, record_size)
		print("Nilai Bfr dari Fixed Blocking adalah: " + str(bfr))

		print("\n======================================================")
		print("|                      Sub Menu                      |")
		print("======================================================")
		print("| 1. Jumlah Block                                    |")
		print("| 2. Pemborosan Ruang (W)                            |")
		print("| 3. Block dan Record Transfer Time                  |")
		print("| 4. Kembali ke Menu Utama                           |")
		print("======================================================")
		pil = input("Masukkan Pilihan: ")
		
		if pil == '1':
			input4 = input("Masukkan jumlah record: ")
			jml_record = int(input4)
			
			jml_block = server.jumlahBlock(jml_record, bfr)
			print("Banyaknya jumlah block adalah: " + str(jml_block))

		elif pil == '2':
			input3 = input("Masukkan gap untuk menghitung pemborosan ruang (dalam byte): ")
			gap = int(input3)
			
			if bfr == 0 :
				print("Tidak Ada Pemborasan ruang karna nilai bfr adalah 0")
			else :
				w = server.W_fixedBfr(bfr,gap)
				print("Nilai Pemborosan ruang Fixing Blocking adalah: " + str(w))

			pil = input("\nApakah anda ingin menghitung Bulk Transfer Rate? (Y/T) ")		
			if pil == 'Y' or pil == 'y':
				input4 = input("Masukkan ukuran transfer rate 't' (dalam byte): ")
				transfer_rate = int(input4)
				input5 = input("Masukkan ukuran record (dalam byte): ")
				record_size = int(input5)
			
				bulk_transferrate = server.bulkTransferrate(transfer_rate, record_size, w)
				print("Nilai Bulk Transfer rate adalah: " + str(bulk_transferrate))
			elif pil == 'T' or pil == 't':
				return 
	
		elif pil == '3':
			ipt = input("Masukkan tansfer rate (dalam ms): ")
			transfer_rate = int(ipt)

			Tr = server.recordTransferTime(record_size, transfer_rate)
			btt = server.blockTransferTime(block_size, transfer_rate)

			print("Nilai Record Transfer Time nya adalah: " + str(Tr))
			print("Nilai Block Transfer Time nya adalah: " + str(btt))
			
		elif pil == '4':
			show_menu()
		else:
			print("Error! Masukkan pilihan sesuai nomor yang ada")
		
	elif pilihan == '2':
		input1 = input("\nMasukkan ukuran block (dalam byte): ")
		block_size = int(input1)
		input2 = input("Masukkan ukuran record (dalam byte): ")
		record_size = int(input2)
		input3 = input("Masukkan ukuran pointer (dalam byte): ")
		pointer_size = int(input3)
		input4 = input("Masukkan ukuran marker (dalam byte): ")
		marker_size = int(input4)
		
		bfr = server.spannedBfr(block_size, record_size, pointer_size, marker_size)
		print("Nilai Bfr dari Spanned Blocking adalah: " + str(bfr))

		print("\n======================================================")
		print("|                      Sub Menu                      |")
		print("======================================================")
		print("| 1. Jumlah Block                                    |")
		print("| 2. Pemborosan Ruang (W)                            |")
		print("| 3. Block dan Record Transfer Time                  |")
		print("| 4. Kembali ke Menu Utama                           |")
		print("======================================================")
		pil = input("Masukkan Pilihan: ")
		
		if pil == '1':
			input5 = input("Masukkan jumlah record: ")
			jml_record = int(input5)
			
			jml_block = server.jumlahBlock(jml_record, bfr)
			print("Banyaknya jumlah block adalah: " + str(jml_block))
		elif pil == '2':
			input3 = input("Masukkan gap untuk menghitung pemborosan ruang (dalam byte): ")
			gap = int(input3)
			if bfr == 0 :
				print("Tidak Ada Pemborasan ruang karna nilai bfr adalah 0")
			else :
				w = server.W_spannedBfr(pointer_size,marker_size,gap,bfr)
				print("Nilai Pemborosan ruang Spanned Blocking adalah: " + str(w))
			pil = input("\nApakah anda ingin menghitung Bulk Transfer Rate? (Y/T) ")		
			if pil == 'Y' or pil == 'y':
				input4 = input("Masukkan ukuran transfer rate 't' (dalam byte): ")
				transfer_rate = int(input4)
				input5 = input("Masukkan ukuran record (dalam byte): ")
				record_size = int(input5)
			
				bulk_transferrate = server.bulkTransferrate(transfer_rate, record_size, w)
				print("Nilai Bulk Transfer rate adalah: " + str(bulk_transferrate))
			elif pil == 'T' or pil == 't':
				return 
		elif pil == '3':
			ipt = input("Masukkan tansfer rate (dalam ms): ")
			transfer_rate = int(ipt)

			Tr = server.recordTransferTime(record_size, transfer_rate)
			btt = server.blockTransferTime(block_size, transfer_rate)

			print("Nilai Record Transfer Time nya adalah: " + str(Tr))
			print("Nilai Block Transfer Time nya adalah: " + str(btt))
		elif pil == '4':
			show_menu()
		else:
			print("Error! Masukkan pilihan sesuai nomor yang ada")
		
	elif pilihan == '3':
		input1 = input("\nMasukkan ukuran block (dalam byte): ")
		block_size = int(input1)
		input2 = input("Masukkan ukuran record (dalam byte): ")
		record_size = int(input2)
		input3 = input("Masukkan ukuran marker (dalam byte): ")
		marker_size = int(input3)
		
		bfr = server.unspannedBfr(block_size, record_size, marker_size)
		print("Nilai Bfr dari Unspanned Blocking adalah: " + str(bfr))

		print("\n======================================================")
		print("|                      Sub Menu                      |")
		print("======================================================")
		print("| 1. Jumlah Block                                    |")
		print("| 2. Pemborosan Ruang (W)                            |")
		print("| 3. Block dan Record Transfer Time                  |")
		print("| 4. Kembali ke Menu Utama                           |")
		print("======================================================")
		pil = input("Masukkan Pilihan: ")
		
		if pil == '1':
			input4 = input("Masukkan jumlah record: ")
			jml_record = int(input4)
			
			jml_block = server.jumlahBlock(jml_record, bfr)
			print("Banyaknya jumlah block adalah: " + str(jml_block))
		elif pil == '2':
			input3 = input("Masukkan gap untuk menghitung pemborosan ruang (dalam byte): ")
			gap = int(input3)
			if bfr == 0 :
				print("Tidak Ada Pemborasan ruang karna nilai bfr adalah 0")
			else :
				w = server.W_unspannedBfr(marker_size,record_size,gap,bfr)
				print("Nilai Pemborosan ruang Unpanned Blocking adalah: " + str(w))
			pil = input("\nApakah anda ingin menghitung Bulk Transfer Rate? (Y/T) ")		
			if pil == 'Y' or pil == 'y':
				input4 = input("Masukkan ukuran transfer rate 't' (dalam byte): ")
				transfer_rate = int(input4)
				input5 = input("Masukkan ukuran record (dalam byte): ")
				record_size = int(input5)
			
				bulk_transferrate = server.bulkTransferrate(transfer_rate, record_size, w)
				print("Nilai Bulk Transfer rate adalah: " + str(bulk_transferrate))
			elif pil == 'T' or pil == 't':
				return 
		elif pil == '3':
			ipt = input("Masukkan tansfer rate (dalam ms): ")
			transfer_rate = int(ipt)

			Tr = server.recordTransferTime(record_size, transfer_rate)
			btt = server.blockTransferTime(block_size, transfer_rate)

			print("Nilai Record Transfer Time nya adalah: " + str(Tr))
			print("Nilai Block Transfer Time nya adalah: " + str(btt))
		elif pil == '4':
			show_menu()
		else:
			print("Error! Masukkan pilihan sesuai nomor yang ada")
		
	elif pilihan == '4':
		input1 = input("\nMasukkan ukuran block (dalam byte): ")
		block_size = int(input1)
		input2 = input("Masukkan ukuran pointer (dalam byte): ")
		pointer_size = int(input2)
		input3 = input("Masukkan ukuran key (dalam byte): ")
		key_size = int(input3)

		fanout = server.fanoutRasio(block_size, pointer_size, key_size)
		print ("Nilai Fanout Rasio adalah: " + str(fanout))

		pil = input("\nApakah anda ingin menghitung block index? (Y/T) ")

		if pil == 'Y' or pil == 'y':
			input4 = input("Masukkan jumlah record: ")
			jml_record = int(input4)
			
			jml_index = server.jumlahIndex(jml_record, fanout)
			print("Banyaknya jumlah block adalah: " + str(jml_index))
		elif pil == 'T' or pil == 't':
			return
	
	elif pilihan == '5':
		exit()
	else:
		print("Error! Masukkan pilihan sesuai nomor yang ada")

while(True):
	show_menu()
